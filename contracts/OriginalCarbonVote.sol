/** The original contract code verbatim was as below:
 * unfortunately we can't use this with a modern solidity compiler. I have attempted to reproduce the original contract with as few changes as possible.
 * 
 * 
 *  contract Vote {
 *     event LogVote(address indexed addr);
 * 
 *     function() {
 *         LogVote(msg.sender);
 * 
 *         if (msg.value > 0) {
 *             msg.sender.send(msg.value);
 *         }
 *     }
 * }
 * 
 */
 
 
pragma solidity ^0.4.24;

contract Vote {
    event LogVote(address indexed addr);
    
    function () public payable {
        emit LogVote(msg.sender);
        
        if (msg.value > 0) {
            msg.sender.send(msg.value);
        }
    }
}

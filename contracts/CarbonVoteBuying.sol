pragma solidity ^0.4.24;

import "./openzeppelin/Ownable.sol";
import "./openzeppelin/StandardToken.sol";
import "./openzeppelin/DetailedERC20.sol";
 
 /** @title CarbonVote Vote Buying Contract */
contract CarbonVoteBuying is Ownable, DetailedERC20, StandardToken {
    /** @dev Provides an easy to use, trustless vote buying mechanism for the CarbonVote contract that was used in the vote during the DAO crisis.
      * @param _voteYesAddress the address of the target CarbonVote yes voting address
      * @param _voteNoAddress the address of the target CarbonVote no voting address
      * @param _name the name of the ERC20 token this contract provides as IOUS for vote buying
      * @param _symbol the symbol of the ERC20 token this contract provides as IOUS for vote buying
      * @param _decimals the number of decimals the token uses - e.g. 8, means to divide the token amount by 100000000 to get its user representation.
      * @param _endingBlockNumber block for which the vote is now over (or at least the bribing phase is over)
      * @param _amountWillingToBribe amount of Wei to bribe for each token issued
      * @param _tokenPeg how much Wei each token ought to be worth
      */
      
    using SafeMath for uint;
    address public voteYesAddress;
    address public voteNoAddress;
    uint public endingBlockNumber;
    uint public amountWillingToBribe;
    uint public bribePot;
    uint public tokenPeg;
    uint public amountOwed;

    event Minted(address who, uint amount);

    /** constructor. **/
    constructor(address _voteYesAddress, address _voteNoAddress, string _name, string _symbol, uint8 _decimals, uint _endingBlockNumber, uint _amountWillingToBribe, uint _tokenPeg) DetailedERC20(_name, _symbol, _decimals) Ownable() public payable {
        bribePot = msg.value;
        voteYesAddress = _voteYesAddress;
        voteNoAddress = _voteNoAddress;
        endingBlockNumber = _endingBlockNumber;
        amountWillingToBribe = _amountWillingToBribe;
        tokenPeg = _tokenPeg;
        amountOwed = 0;
    }
    
    /** allows the owner or other person to increase the amount of money avaliable for bribing third parties */
    function addToBriberyPot() public payable {
        require(block.number < endingBlockNumber);
        bribePot = bribePot.add(msg.value);
    }
    
    /** Any party can call this function to pay out the IOUs when the vote is over. */
    function redeemIOU() public {
        require(block.number > endingBlockNumber);
        uint256 messageSenderAmount = balances[msg.sender];
        require(messageSenderAmount > 0);
        uint256 bribeSize = messageSenderAmount.mul(tokenPeg).add(messageSenderAmount.mul(amountWillingToBribe));
        assert(bribePot > bribeSize);
        balances[msg.sender] = 0;
        msg.sender.transfer(bribeSize);
    }
    
    /** a willing third party can call this function to generate an IOU, though they should definitely make msg.value a multiple of tokenpeg, or they will lose money from truncated division */
    function mint() public payable {
        require(block.number < endingBlockNumber);
        require(msg.value > tokenPeg);
        uint numTokens = msg.value.div(tokenPeg);
        amountOwed = numTokens.mul(tokenPeg).add(numTokens.mul(amountWillingToBribe)).add(amountOwed);
        require(amountOwed < bribePot);
        balances[msg.sender] = numTokens;
        emit Minted(msg.sender, numTokens);
    }
    
    /**Owner calls this before the vote ends to cast votes in the desired way */
    function voteYes() public onlyOwner() {
        require(block.number < endingBlockNumber);
        voteYesAddress.transfer(0);
    }

    /**Owner calls this before the vote ends to cast votes in the desired way */
    function voteNo() public onlyOwner {
        require(block.number < endingBlockNumber);
        voteNoAddress.transfer(0);
    }
}

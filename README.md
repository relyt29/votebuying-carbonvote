# Vote Buying CarbonVote Contract

## Buying Votes

[CarbonVote](http://v1.carbonvote.com/) was an Ethereum smart contract implementation of a voting system that was used during the DAO hack to allow the community to voice what action should be taken (to reverse the hack or not). In this code repo, we show a proof of concept smart contract implementation that could have been used to buy votes to influence the original DAO election.

## Authors

We are a team of blockchain researchers:

* Tyler K (Cornell Tech, IC3)
* Phil Daian (Cornell Tech, IC3)

with advice, review, and support from Ari Juels (Cornell Tech, IC3, The Jacobs Institute).

## Disclaimer

This is a proof of concept implementation. It has not been thoroughly tested for security and may not be ready for production.

We offer absolutely no support, guarantees, advice, or other help with this software. If you like it, use it.

THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESSED OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
